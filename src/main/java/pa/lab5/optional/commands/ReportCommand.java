package pa.lab5.optional.commands;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import pa.lab5.compulsory.catalog.Catalog;

import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ReportCommand extends Command {

    Catalog catalog;

    public ReportCommand(Catalog catalog) {
        this.catalog = catalog;
    }

    public void execute() {

        Configuration cfg = new Configuration(Configuration.VERSION_2_3_23);

        try {
            cfg.setDirectoryForTemplateLoading(new File(Paths.get("", "src/main/java/pa/lab5/optional/template").toString()));
        } catch (Exception e) {
            System.out.println(e);
        }
        cfg.setDefaultEncoding("UTF-8");
        cfg.setLocale(Locale.US);
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);


        Map<String, Object> input = new HashMap<String, Object>();

        input.put("title", "Content of the catalog");
        input.put("system", this.catalog.getItems());

        Template template = null;
        try {
            template = cfg.getTemplate("item.ftl");
        } catch (Exception e) {
            System.out.println(e);
        }

        try {
            Writer fileWriter = new FileWriter(new File("output.html"));
            template.process(input, fileWriter);
            fileWriter.close();
        } catch (Exception e) {
            System.out.println(e);
        }

        Desktop desktop = Desktop.getDesktop();
        File f = new File("output.html");

        try {
            desktop.open(f);
        } catch (IOException e) {
            System.out.println(e);
        }

    }
}
