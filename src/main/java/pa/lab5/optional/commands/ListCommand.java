package pa.lab5.optional.commands;

import pa.lab5.compulsory.catalog.Catalog;

public class ListCommand extends Command {
    Catalog catalog;

    public ListCommand(Catalog catalog) {
        this.catalog = catalog;
    }

    public void execute() {
        System.out.println(catalog);
    }
}

