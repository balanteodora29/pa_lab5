package pa.lab5.optional.commands;

import pa.lab5.compulsory.catalog.Catalog;

import java.awt.*;
import java.io.File;
import java.io.IOException;

public class PlayCommand extends Command {

    Catalog catalog;
    String path;

    public PlayCommand(Catalog catalog) {
        this.catalog = catalog;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void execute() {
        if (Desktop.isDesktopSupported()) {
            try {
                File myFile = new File(this.path);
                Desktop.getDesktop().open(myFile);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

}