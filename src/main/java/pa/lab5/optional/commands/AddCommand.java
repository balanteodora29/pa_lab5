package pa.lab5.optional.commands;

import pa.lab5.compulsory.catalog.Catalog;
import pa.lab5.compulsory.catalog.Item;

public class AddCommand extends Command {
    Catalog catalog;
    Item item;

    public AddCommand(Catalog catalog) {
        this.catalog = catalog;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public void execute() {
        catalog.add(item);
    }

}
