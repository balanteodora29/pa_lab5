package pa.lab5.optional.commands;

import pa.lab5.compulsory.catalog.Catalog;

public class SaveCommand extends Command{
    Catalog catalog;

    public SaveCommand(Catalog catalog) {
        this.catalog = catalog;
    }

    public void execute() {
        catalog.save(catalog);
    }
}
