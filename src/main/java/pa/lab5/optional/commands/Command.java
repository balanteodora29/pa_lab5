package pa.lab5.optional.commands;

public abstract class Command {
    public abstract void execute();
}
