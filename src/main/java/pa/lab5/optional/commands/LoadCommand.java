package pa.lab5.optional.commands;

import pa.lab5.compulsory.catalog.Catalog;

public class LoadCommand extends Command {

    Catalog catalog;

    public LoadCommand(Catalog catalog) {
        this.catalog = catalog;
    }

    public void execute() {
        String path = catalog.getPath();
        catalog.load(path);
    }
}
