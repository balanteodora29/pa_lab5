package pa.lab5.optional;

import pa.lab5.compulsory.catalog.Book;
import pa.lab5.compulsory.catalog.Catalog;
import pa.lab5.compulsory.catalog.Movie;
import pa.lab5.optional.commands.*;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Catalog catalog = new Catalog("C://Users//teodo//Desktop/catalog/catalog/catalog.ser");
        AddCommand add = new AddCommand(catalog);
        PlayCommand play = new PlayCommand(catalog);
        ListCommand list = new ListCommand(catalog);
        SaveCommand save = new SaveCommand(catalog);
        LoadCommand load = new LoadCommand(catalog);
        ReportCommand report = new ReportCommand(catalog);

        boolean isRunning = true;
        while(isRunning) {
            System.out.println("Please enter your command: ");
            Scanner scanner = new Scanner(System.in);
            String command = scanner.nextLine();
            String[] tokens = command.split(" ");
            int commandDimension = tokens.length;

            switch(tokens[0]) {
                case "add":
                    switch(tokens[1]) {
                        case "book":
                            String bookTitle = new String();
                            for (int i = 2; i < commandDimension; i++) {
                                bookTitle += tokens[i] + " ";
                            }
                            add.setItem(new Book(bookTitle));
                            add.execute();
                            break;

                        case "movie":
                            String movieTitle = new String();
                            for (int i = 2; i < commandDimension; i++) {
                                movieTitle += tokens[i] + " ";
                            }
                            add.setItem(new Movie(movieTitle));
                            add.execute();
                            break;

                        default:
                            System.out.println("This item cannot be added into the catalog!");
                    }
                    break;

                case "list":
                    switch(tokens[1]) {
                        case "catalog":
                            list.execute();
                            break;

                        default:
                            System.out.println("Only catalog can be listed!");
                    }
                    break;

                case "save":
                    save.execute();
                    break;

                case "load":
                    load.execute();
                    break;

                case "play":
                    play.setPath(tokens[1]);
                    play.execute();
                    break;

                case "report":
                    report.execute();

                case "exit":
                    isRunning = false;
                    break;
            }
        }
    }
}
