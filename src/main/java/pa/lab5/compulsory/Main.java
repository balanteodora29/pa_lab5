package pa.lab5.compulsory;

import pa.lab5.compulsory.catalog.Book;
import pa.lab5.compulsory.catalog.Catalog;
import pa.lab5.compulsory.catalog.Item;
import pa.lab5.compulsory.catalog.Movie;

public class Main {
    static public void main(String[] args){
        Catalog catalog = new Catalog();
        catalog.setPath("C://Users//teodo//Desktop/catalog/catalog/catalog.ser");

        Item book = new Book("Unshakable", "C://Users//teodo//Desktop/catalog/book1.pdf", "Tony Robbins");
        Item movie = new Movie("Frozen", "C://Users//teodo//Desktop/catalog/movie1.mp4", "animated");

        catalog.add(book);
        catalog.add(movie);
        System.out.println(catalog.list());
        catalog.play(movie);
        catalog.save(catalog);
        catalog.load(catalog.getPath());
    }
}