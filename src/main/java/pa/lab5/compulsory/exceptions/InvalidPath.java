package pa.lab5.compulsory.exceptions;

public class InvalidPath extends Exception {
    public InvalidPath (Exception ex){
        super("Invalid path !!", ex);
    }
}
