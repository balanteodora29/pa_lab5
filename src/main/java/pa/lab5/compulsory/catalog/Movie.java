package pa.lab5.compulsory.catalog;

import java.io.Serializable;

public class Movie extends Item implements Serializable {
    private String genre;
    private String rating;

    public Movie(String name){
        super(name);
    }

    public Movie(String name, String path, String genre) {
        super(name, path);
        this.genre = genre;
    }

    // setters and getters

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "genre='" + genre + '\'' +
                ", rating='" + rating + '\'' +
                '}';
    }
}
