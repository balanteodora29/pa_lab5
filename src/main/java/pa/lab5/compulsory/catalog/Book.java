package pa.lab5.compulsory.catalog;

import pa.lab5.compulsory.catalog.Item;

import java.io.Serializable;

public class Book extends Item implements Serializable {
    private String author;
    private int nrOfPages;
    private int releaseYear;

    public Book(String name){
        super(name);
    }

    public Book(String name, String path, String author) {
        super(name, path);
        this.author = author;
    }


    //setters and getters

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getNrOfPages() {
        return nrOfPages;
    }

    public void setNrOfPages(int nrOfPages) {
        this.nrOfPages = nrOfPages;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    @Override
    public String toString() {
        return "Book{" +
                "author='" + author + '\'' +
                ", nrOfPages=" + nrOfPages +
                ", releaseYear=" + releaseYear +
                '}';
    }
}

