package pa.lab5.compulsory.catalog;

import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Catalog implements Serializable {
    private List<Item> items = new ArrayList<>();
    private String path;

    public Catalog() {}

    public Catalog(String path) {
        this.path = path;
    }

    public Catalog(List<Item> items, String path) {
        this.items = items;
        this.path = path;
    }

    // setters and getters

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<Item> getItems() { return items; }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    /**
     * The function adds an element to the list of items
     * @param item
     */
    public void add(Item item){
        items.add(item);
    }

    /**
     * The function prints the elements of the catalog
     * @return
     */
    public String list(){
        return "Catalog{" +
                "items=" + items +
                '}';
    }

    /**
     * The function opens/plays an Item object
     * @param item
     */
    public void play(Item item){
        if (Desktop.isDesktopSupported()) {
            try {
                File myFile = new File(item.getPath());
                Desktop.getDesktop().open(myFile);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * The function saves the data from the catalog in the given path
     * @param catalog
     */
    public void save(Catalog catalog) {
        try {
            FileOutputStream fileOut = new FileOutputStream(catalog.getPath());
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(catalog);
            out.close();
            fileOut.close();
        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    /**
     * This function loads the info from a given path to the current Catalog object
     * @param absolutePath
     */
    public void load(String absolutePath) {
        Catalog reloadCatalog;
        try {
            FileInputStream fileIn = new FileInputStream(absolutePath);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            reloadCatalog = (Catalog) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException i) {
            i.printStackTrace();
            return;
        } catch (ClassNotFoundException c) {
            System.out.println("Catalog class not found");
            c.printStackTrace();
            return;
        }
        System.out.println(reloadCatalog);
    }
}
